# dHondt Open list
## Goal
Create a function in r to give information about the results of an election that uses the system d'Hondt with the open lists variant.

## Arguments of the function  
*dHondtOpen(openList,seat,year,place,chamber,threshold)*  

1. *openList*: data.frame with the data (see below).  
2. *seat* (number): the total number of seats disputed in the election.  
3. *year* (number): The year of the election.  
4. *place* (quoted character): city, province or country in which the election takes place.  
5. *chamber* (quoted character): name of the house of the representatives.  
6. *threshold* (number): Legal minimum to achieve to participate in the distribution of seats. Must be expressed as proportion (between 0 and 1), not as a percentage.  

It is possible to use the example included [here](https://gitlab.com/ortizpalanques1/dhondt-open-list/blob/master/test02.xlsx). It has to be read with, for example, *library(readit), fileDF<-readit("test02.xlsx")* . Then, use the name of the data.frame as the first argument in the function.

## Data Frame
Data need to be in data.frame format with the following vectors:  

1. IDCandidate: numeric, id of each candidate  
2. ID: Id for each party  
3. Party: Name of the political party  
4. Name: name of the candidate  
5. Colour: distinctive color of the party  
6. Position: place of the candidate inside the party list  
7. Vote: number of votes received for each candidate  

## The function
Electoral systems based on the d'Hondt formula work not only for closed lists, but also for open lists. In this version of the system, instead of only having the possibility of selecting one list, voters can choose which candidates will they vote for. Later, votes of the candidates for each party are summed up and the seats are distributed according the d'Hondt systems. Finally, depending on the number of seats achieved by each party, candidates are selected: first the candidate who received more votes in the list and so on.  
This function has four products:  
1. A .txt file containing the names of the parties which have obtained representatives, their vote, the number of representatives and their vote share. This file can be converted into a table using MSWord. 
2. A .png file with the graphical representation of the final result.  
3. A .txt file with the elected representatives. This file can be converted into a table using MSWord. 
4. A .txt file with a brief report on the election.

## Tests
This function has been tested against relevant examples. However, not all the possible cases have been considered. Therefore, we cannot guarantee full accuracy.

## Acknowledgments
1. The package [ggparliament](https://github.com/robwhickman/ggparliament) by Rob W. Hickman.
2. [tidyverse](https://www.tidyverse.org/) and its packages [dplyr](https://dplyr.tidyverse.org/)  and [ggplot](https://ggplot2.tidyverse.org/).
3. Data.frames for testing purposes were created using numbers from [random.org](https://www.random.org), names from a [random name generator](https://randomwordgenerator.com/name.php), and political party names from a [random party name generator](https://fantasynamegenerators.com/political-party-names.php).